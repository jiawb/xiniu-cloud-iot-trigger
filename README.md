  
  
# 框架图  
![](http://118.122.93.190:10080/xiniu-cloud/docs/raw/master/images/iot-trigger/%E6%A1%86%E6%9E%B6%E5%9B%BE.png)  
# 组件功能  
- 设备数据满足触发条件时，向平台发出消息。   
  
# 依赖关系  
组件在运行时，依赖ETCD服务端、kafka服务端运行    
- 组建需从ETCD服务端中拉取和观察key前缀为 `/registry/triggers/project/` 的所以数据——value。value中的数据是触发事件的规则。     
- 组建需从ETCD服务端中拉取和观察key前缀为 `iot-trigger/template/` 的所以数据——value。value中的数据是消息模板，根据消息模板生成消息内容（暂未实现）。      
- 组件需要从kafka中的 topic `devicedata-clean` 中拉取设备数据；生成了消息，会放进kafka中的对应的topic中。  
  
# 开发  
- 开发环境需要安装maven 3.2或以上版本  
- oracle JDK 1.8或以上版本。  
   
# 快速部署  
  
1. 环境配置  

```bash
[trigger]$ cd src/main/resources/  
[resources]$ vim application.properties  
```

在配置项的说明如下    
  
| key | 类型 | 说明 | 举例 |  
| - | :-: | :-: | -: |  
| etcd_endpoints | string | etcd服务端的链接地址 | http://172.168.1.240:2379 |  
| trigger.thread.pool.size | int | 触发器线程池的大小 | 20 |  
| kafka_consumer_bootstrap_servers | string | kafka消费者的ip和端口 | 172.168.1.233:9092或者172.168.1.233:9092,172.168.1.233:9093,172.168.1.233:9094 |  
| kafka_producer_bootstrap_servers | string | kafka生产者的ip和端口 | 172.168.1.233:9092或者172.168.1.233:9092,172.168.1.233:9093,172.168.1.233:9094 |  
| kafka.consumer.topic | string | kafka消费者topic | devicedata-clean |  
| kafka.consumer.topic | string | kafka生产者topic | devicedata-alert |  
    
2. 打包 (跳过项目中的测试代码),具体情况请根据maven命令进行操作  

```bash  
[trigger]$  mvn package -Dmaven.test.skip=true  
```  

3. 运行  

```shell  
[trigger]$  nohup java -jar target/iot-trigger-0.0.1-SNAPSHOT.jar > log.log &  
```  

4. 查看日志  

```shell  
[trigger]$  tail -f target/log.log  
```  
  
# 触发规则 #  
进入etcd客户端进行命令操作  

+ 查看所有的触发规则  

    ```bash  
    $  etcdctl get /registry/triggers/project/ --prefix
    ``` 

+ 消息发送至数据总线，需要准许ETCD中的消息规则，如下的ETCD中的某个key
    
    ```
    /registry/messages/project/{project_id}/{topic}/{partition}/<rule_name>
    ```
    + 以上的key说明：将项目ID为`project_id`下设备产生的消息，发送到kafka中的`topic`中，选择的`partition`为`{partition}`
    
+ 触发规则key设置:
    
    ```
    /registry/triggers/project/{project_id}/{rule_name}
    ```

+ 触发规则字段表达式，etcd中拉取的数据格式   
      
    ```json
    {
        "expr": {
            "uncap": {
                "sins":[{"gt": 0},{"leq":3}],
                "rp":"0 AND 1"
            },
            "xValue":{
                "sins":[{"gt": 0},{"leq":3}],
                "rp":"0 AND 1"
            }
        },
        "rp":"OR",
        "modelId":"9e9cc553-f4d3-48d7-9926-041284309339",
        "includeIds":[],
        "excludeIds":[],
        "serverity":  "Warning",
        "templateId": "templateId-1",
        "msg": "xxxx{{uncap}}xxxx",
        "name":"trigger3",
        "multi_alert":1
    }
     ```

+ 规则说明  
    + `expr`中定义了需要进行判断的字段，有两个部分key分别是 `sins`和`rp`
        + `sins`该字段的多种判断方式的数组，比如 `[{"gt":0},{"leq":3}]`,索引0对象表达大于于0，索引1的对象表达小于或等于3
            + `gt`代表大于
            + `lt`代表小于
            + `eq`代表相等
            + `neq`代表不相等
            + `geq`代表大于或则等于
            + `leq`代表小于或则等于
            + `lt-delta`和`gt-delta`代表变化超过的范围时，就会触发。
                + 当数值向上变化超过设定的值时`gt-delta`会触发`DeviceAlert`，当变化范围恢复时，会触发`DeviceRecovery`。
                + 当数值向下变化超过设定的值时`lt-delta`会触发`DeviceAlert`，当变化范围恢复时，会触发`DeviceRecovery`。
            + （已废除）`sigma`。
            + （已废除）`nsigma`和`psigma`
        + `rp`表示了每个表达式之间的关系比如`0 AND 1`,结合`sins`一起理解，表达的意思是：该字段如果值是大于0且小于等于3时，计算该字段的表达式结果为true
    + `rp`表示字段与字段之间的关系`OR`是或的关系，`AND`是与的关系,并且支持简单的表达式，比如: `field1 AND (field2 OR field3)`
    + `templateId`中定义了，条件满足出发后生生的消息提内容模板。（暂未实现模板定制消息内容的功能)    
    + `msg`中则是消息体中的 `msg` 字段中的内容,其中双花括号`{{}}`中可以填写需要显示的字段的值，比如msg的值是`我想看看uncap的值，值为：{{uncap}}`，那么出发告警后，得到的msg中的内容为`我想看看uncap的值，值为：1`
    + `serverity` 告警级别，包括`Private` `Information` `Warnning` `Averag` `High` `Critical` `Disaster`
    + `includeIds`需要准守这条规则的设备ID
    + `excludeIds`该组件，展示还不检查该字段
    + `multi_alert` 表示是否需要重复发送告警消息，`0`---不需要重复发送告警  `非0`---需要重复发送告警

+ 举例

    + key设置:如下设置了`projectID`为`10000`项目中的所有设备都遵循`rule_name_1`中的处罚规则
    
    ```
    /registry/triggers/project/10000/rule_name_1
    ```

    + value设置：如下的json数据表示。
    
    ```json
    规则内容如上述的json，表示当 uncap 的值 或则 xValue的值 大于0且小于等于3，时会触发该条规则，并发送事件消息。
    ```

+ 操作规则  
    + 下面的命令则是修改或者添加规则的命令。 
    
    ```bash  
    $  etcdctl put /registry/triggers/project/10000/rule_name_1 '{}'
    ```  
    + 删除规则
    
    ```bash
    $  etcdctl del /registry/triggers/project/10000/rule_name_1
    ```
    

+ 消息模板（暂未实现）  
    + 消息中数据字段（data）的模板，etcd中拉取的数据格式
    
    ```  
     iot-trigger/template/templateId-1 = "field-1,field-2,fielt3"
    ```   
 
+ 发送到kafka中的消息  
    + 发送到kafka的消息内容,data字段是根据模板Id中的内容而定。可以通过日志中查看，数据格式如下：

    ```json
    {
        "rule": <PlateformBuild>/<ruleName>,
        "type": "DeviceAlert/DeviceRecovery",
        "time": 1578965231254,
        "serverity": "",
        "expersion": '{}',
        "producer": "iot-trigger",
        "data": '{"device": "", status": ""}',
        "msg":"xxxxxxxxxxxx"
    }
    ```
