package com.xiguthings.xiniu.iot.trigger.worker.etcd;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import com.xiguthings.xiniu.iot.etce.ProjectMessageEtcdClient;
import com.xiguthings.xiniu.iot.etce.TemplateEtcdClient;
import com.xiguthings.xiniu.iot.etce.TriggerEtcdClient;

@Component
public class EtcdBeanCreater {
	@Value("${etcd_endpoints}")
	private String etcdEndpoints;
	@Value("${etcd.trigger.messages.key.keyword}")
	private String projectMessageKeyWord;
	@Value("${etcd.trigger.rule.key.keyword}")
	private String triggerRulesKeyWord;
	@Value("${trigger.data.template.keyword}")
	private String triggerDataTemplateKeyWord;

	@Bean
	public ProjectMessageEtcdClient projectMessageEtcdClient() {
		ProjectMessageEtcdClient projectMessageEtcdClient = new ProjectMessageEtcdClient(projectMessageKeyWord,
				etcdEndpoints);
		return projectMessageEtcdClient;
	}

	@Bean
	public TriggerEtcdClient triggerEtcdClient() {
		TriggerEtcdClient triggerEtcdClient = new TriggerEtcdClient(triggerRulesKeyWord, etcdEndpoints);
		return triggerEtcdClient;
	}

	@Bean
	public TemplateEtcdClient templateEtcdClient() {
		TemplateEtcdClient templateEtcdClient = new TemplateEtcdClient(triggerDataTemplateKeyWord, etcdEndpoints);
		return templateEtcdClient;
	}
}
