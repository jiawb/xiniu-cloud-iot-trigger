package com.xiguthings.xiniu.iot.trigger.worker.trigger.calculation;

public interface Engine {
	boolean expressionCalculation(String expStr) throws Exception;
}
