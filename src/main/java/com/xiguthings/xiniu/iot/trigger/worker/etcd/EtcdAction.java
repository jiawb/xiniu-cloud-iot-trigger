package com.xiguthings.xiniu.iot.trigger.worker.etcd;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

import com.xiguthings.xiniu.iot.etce.ProjectMessageEtcdClient;
import com.xiguthings.xiniu.iot.etce.TemplateEtcdClient;
import com.xiguthings.xiniu.iot.etce.TriggerEtcdClient;

@Component
public class EtcdAction {

	@Autowired
	private ProjectMessageEtcdClient projectMessageEtcdClient;
	@Autowired
	private TriggerEtcdClient triggerEtcdClient;
	@Autowired
	private TemplateEtcdClient templateEtcdClient;

	@PostConstruct
	private void action() {
		projectMessageEtcdClient.action();
		triggerEtcdClient.action();
		templateEtcdClient.action();
	}

}
